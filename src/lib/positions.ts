import * as THREE from "three";

interface PaintingPosition {
  position: THREE.Vector3;
  rotation: THREE.Euler;
}

// The positions of paintings are the same for all nodes
export const positions: PaintingPosition[] = [
  {
    position: new THREE.Vector3(1, 1, 2),
    rotation: new THREE.Euler(-Math.PI / 4, Math.PI / 7, Math.PI / 8),
  },
  {
    position: new THREE.Vector3(1, -1, 1),
    rotation: new THREE.Euler(Math.PI / 4, Math.PI / 6, 0),
  },
  {
    position: new THREE.Vector3(-1, 1, 1),
    rotation: new THREE.Euler(-Math.PI / 4, -Math.PI / 3, 0),
  },
  {
    position: new THREE.Vector3(0, 0, -1),
    rotation: new THREE.Euler(-1 * Math.PI, 2 * Math.PI, Math.PI),
  },
  {
    position: new THREE.Vector3(1, -1.5, -1),
    rotation: new THREE.Euler(2, 0.5, -2),
  },
  {
    position: new THREE.Vector3(-1, -1, 1),
    rotation: new THREE.Euler(1, -0.5, 1),
  },
];

import { prisma } from "@/lib/prisma";
import { positions } from "../../../../lib/positions";
import { NextResponse } from "next/server";
import { getNode } from "@/lib/nodes";

interface Node {
  position: THREE.Vector3;
  paintings: PaintingProps[];
  neighbours: number[];
}

interface PaintingProps {
  url: string;
  name: string;
  position: THREE.Vector3;
  rotation: THREE.Euler;
  height: number;
  width: number;
}

interface GetProps {
  params: {
    id: number;
  };
}

export const GET = async (request: Request, { params }: GetProps) => {
  // const paintings = await getNode(Number(params.id));
  // console.log("node.GET");

  // return NextResponse.json({ paintings });
  return NextResponse.json({ eh: "man" });
};

"use client";

import Cable from "@/components/Cable";
import { Sphere } from "@react-three/drei";
import { ThreeEvent, useFrame, useThree } from "@react-three/fiber";
import { MutableRefObject, Ref, useMemo, useRef, useState } from "react";
import * as THREE from "three";
import { CameraControls } from "@react-three/drei";

// CameraControls.install({ THREE });

export default function Nodes() {
  const [clickedNode, setClickedNode] = useState<THREE.Object3D<THREE.Event>>();
  const cameraRef = useRef<CameraControls>(null);

  const onClick = (evt: ThreeEvent<MouseEvent>) => {
    setClickedNode(evt.object);
  };

  const deClick = () => {
    setClickedNode(undefined);
  };

  const camera = useThree((state) => state.camera);
  const gl = useThree((state) => state.gl);
  // const controls = useMemo(() => new CameraControls(camera, gl.domElement), []);

  useFrame((state, delta) => {
    let pos: THREE.Vector3;
    let cameraPos;

    if (clickedNode) {
      pos = clickedNode.position || new THREE.Vector3(0, 0, 0);
    } else {
      pos = new THREE.Vector3(0, 0, 0);
    }

    cameraPos = new THREE.Vector3(pos.x, pos.y, pos.z);
    cameraRef.current?.moveTo(cameraPos.x, cameraPos.y, cameraPos.z, true);

    return null;
  });

  return (
    <>
      <group onPointerMissed={deClick}>
        <Node
          id={"0"}
          onClick={onClick}
          color={new THREE.Color(1, 0, 0)}
          position={new THREE.Vector3(10, 0, 0)}
        />
        <Cable start={[10, 0, 0]} end={[0, 0, 0]} />
        <Node
          id={"1"}
          onClick={onClick}
          color={new THREE.Color(0, 1, 0)}
          position={new THREE.Vector3(-10, 0, 0)}
        />
        <Cable start={[-10, 0, 0]} end={[0, 0, 0]} />
        <Node
          id={"1"}
          onClick={onClick}
          color={new THREE.Color(0, 0, 1)}
          position={new THREE.Vector3(0, 10, 0)}
        />
        <Cable start={[0, 10, 0]} end={[0, 0, 0]} />
        <Node
          id={"1"}
          onClick={onClick}
          color={new THREE.Color(1, 1, 0)}
          position={new THREE.Vector3(0, -10, 0)}
        />
        <Cable start={[0, -10, 0]} end={[0, 0, 0]} />
        <Node
          id={"1"}
          onClick={onClick}
          color={new THREE.Color(1, 0, 1)}
          position={new THREE.Vector3(0, 0, 10)}
        />
        <Cable start={[0, 0, 10]} end={[0, 0, 0]} />
        <Node
          id={"1"}
          onClick={onClick}
          color={new THREE.Color(0, 1, 1)}
          position={new THREE.Vector3(0, 0, -10)}
        />
        <Cable start={[0, 0, -10]} end={[0, 0, 0]} />
      </group>
      <CameraControls ref={cameraRef} camera={camera} />
    </>
  );
}

const Node = ({
  id,
  onClick,
  color,
  position,
}: {
  id: string;
  onClick: (evt: ThreeEvent<MouseEvent>) => void;
  color: THREE.Color;
  position: THREE.Vector3;
}) => {
  return (
    <mesh onClick={onClick} name={id}>
      <Sphere position={position} scale={new THREE.Vector3(0.5, 0.5, 0.5)}>
        <meshBasicMaterial color={color} toneMapped={false} />
      </Sphere>
    </mesh>
  );
};

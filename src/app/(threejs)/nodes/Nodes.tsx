"use client";

import * as THREE from "three";
import { CameraControls, Sphere } from "@react-three/drei";
import Cable from "@/components/Cable";
import { Fragment, useRef, useState } from "react";
import { ThreeEvent, useFrame, useThree } from "@react-three/fiber";
import { useRoute, useLocation } from "wouter";
import { getPaintings } from "@/lib/paintings";
import Painting from "@/components/Painting";
import { NodeProps, PaintingProps } from "@/types";
import NodeSphere from "@/components/NodeSphere";

const Nodes = ({ nodes }: { nodes: NodeProps[] }) => {
  const [activeNode, setActiveNode] = useState<string>("");
  const cameraRef = useRef<CameraControls>(null);

  const [clickedNode, setClickedNode] = useState<THREE.Object3D<THREE.Event>>();

  const gl = useThree((state) => state.gl);
  const camera = useThree((state) => state.camera);

  const click = (evt: ThreeEvent<MouseEvent>) => {
    evt.stopPropagation();
    console.log("redirecting to", evt.object.name);
    setActiveNode(evt.object.name);
  };

  const deClick = () => {
    setActiveNode("0");
  };

  console.log("rendering nodes");

  useFrame((state, delta) => {
    let pos: THREE.Vector3;
    let cameraPos;

    if (activeNode !== "") {
      const targetNode = nodes.find((n) => n.id === Number(activeNode));
      const posArray = targetNode ? targetNode.position : [0, 0, 0];
      pos =
        new THREE.Vector3(posArray[0], posArray[1], posArray[2]) ||
        new THREE.Vector3(0, 0, 0);
    } else {
      pos = new THREE.Vector3(0, 0, 0);
    }

    cameraPos = new THREE.Vector3(pos.x, pos.y, pos.z);

    cameraRef.current?.moveTo(cameraPos.x, cameraPos.y, cameraPos.z, true);
  });

  return (
    <>
      <group onPointerMissed={deClick}>
        {nodes.map((node) => (
          <Fragment key={node.id}>
            <NodeSphere
              position={node.position}
              id={node.id}
              click={click}
              active={node.id.toString() === activeNode}
              paintings={node.paintings}
            />
            {node.neighbours.map((neighbour, nIdx) => (
              <Fragment key={nIdx}>
                <Cable start={node.position} end={nodes[neighbour].position} />
              </Fragment>
            ))}
          </Fragment>
        ))}
      </group>
      <CameraControls ref={cameraRef} camera={camera} />
    </>
  );
};

export default Nodes;

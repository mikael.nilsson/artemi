import { Vector2, Vector3, Euler } from "three";
import { positions } from "./positions";
import { prisma } from "./prisma";

export interface PaintingProps {
  url: string;
  name: string;
  position: Vector3;
  rotation: Euler;
  height: number;
  width: number;
  frameSize?: Vector2;
}

export const getPaintings = async (nodeId: number) => {
  const dbNode = await prisma.node.findUnique({
    where: { id: Number(nodeId) },
  });

  const paintings: PaintingProps[] = [];
  const nodePosition = new Vector3(
    dbNode?.position[0],
    dbNode?.position[1],
    dbNode?.position[2]
  );

  if (dbNode?.paintings) {
    const paintingIds = dbNode.paintings;
    for (let i = 0; i < paintingIds.length; i++) {
      const paintingObject = await prisma.painting.findUnique({
        where: { id: paintingIds[i] },
      });

      const paintingPosition = addVector3(positions[i].position, nodePosition);

      paintingObject &&
        paintings.push({
          ...paintingObject,
          position: paintingPosition,
          rotation: positions[i].rotation,
        });
    }
  }

  return paintings;
};

const addVector3 = (a: Vector3, b: Vector3): Vector3 => {
  return new Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
};

/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ['drive.google.com', 'images.pexels.com']

  }
}

module.exports = nextConfig

"use client";

import { Canvas } from "@react-three/fiber";
import { XR } from "@react-three/xr";

export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <>
      <Canvas shadows camera={{ position: [5, 2, 10], fov: 50 }}>
        <XR referenceSpace="local">{children}</XR>
      </Canvas>
    </>
  );
}

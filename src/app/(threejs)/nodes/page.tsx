import { getNodes } from "@/lib/nodes";
import Nodes from "./Nodes";
import { CameraControls } from "@react-three/drei";

const Page = async () => {
  const nodes = await getNodes();

  return (
    <>
      <Nodes nodes={nodes} />
    </>
  );
};

export default Page;

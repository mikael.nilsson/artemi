"use client";

import * as THREE from "three";
import { Canvas, ThreeElements, useFrame, useLoader } from "@react-three/fiber";
import { XR, XRButton, ARButton, Controllers, VRButton } from "@react-three/xr";
import { redirect } from "next/navigation";
import {
  Billboard,
  CameraControls,
  Cylinder,
  Lightformer,
  MeshReflectorMaterial,
  MeshTransmissionMaterial,
  Sphere,
} from "@react-three/drei";

import Painting from "../components/Painting";
import { useRef } from "react";
// import Cable from "@/components/Cable";
// import Tetradotoxin from "./Tetradotoxin";

// https://codesandbox.io/s/vkgi6?file=/src/index.js

const Home = () => {
  const light = useRef<THREE.PointLight>(new THREE.PointLight());
  const stripe = useRef(null);

  light.current.intensity = 10;

  redirect("/0?nodeId=0");

  // return (
  //   <>
  //     <Canvas
  //       frameloop="demand"
  //       shadows
  //       camera={{ position: [5, 2, 10], fov: 50 }}
  //     >
  //       <CameraControls makeDefault />
  //       <XR referenceSpace="local">
  //         <Sphere castShadow scale={[0.3, 0.3, 0.3]} position={[0, 0, 0]}>
  //           <pointLight distance={60} intensity={4} color="white" />
  //           <meshBasicMaterial color={[0.5, 0.5, 0.5]} toneMapped={false} />
  //         </Sphere>

  //         <Painting
  //           url="https://drive.google.com/uc?export=view&id=1FiLIXabD_XtY6BsScu_8HtLbIGqp4HtN"
  //           name="Valborg"
  //           position={new THREE.Vector3(1, 1, 2)}
  //           rotation={new THREE.Euler(-PI / 4, PI / 7, PI / 8)}
  //           imageScale={[1704, 2190]}
  //         />

  //         {/*
  //         <Painting
  //           url="deathblob.jpg"
  //           name="Tetradotoxin"
  //           position={new THREE.Vector3(1, -1, 1)}
  //           rotation={new THREE.Euler(PI / 4, PI / 6, 0)}
  //           // rotation={new THREE.Euler(0, 0, 0)}
  //         />

  //         <Painting
  //           url="valborg.jpg"
  //           name="10"
  //           position={new THREE.Vector3(-1, 1, 1)}
  //           rotation={new THREE.Euler(-PI / 4, -PI / 3, 0)}
  //           // rotation={new THREE.Euler(0, 0, 0)}
  //         />
  //         <Painting
  //           url="valborg.jpg"
  //           name="11"
  //           position={new THREE.Vector3(1, 1, -1)}
  //           rotation={new THREE.Euler(-3 * PI, 2 * PI, PI / 3)}
  //           // rotation={new THREE.Euler(0, 0, 0)}
  //         />

  //         <Painting
  //           url="valborg.jpg"
  //           name="12"
  //           position={new THREE.Vector3(1, -1, -1)}
  //           rotation={new THREE.Euler(2, 1, -1)}
  //         />
  //         <Painting
  //           url="valborg.jpg"
  //           name="13"
  //           position={new THREE.Vector3(-1, -1, 1)}
  //           rotation={new THREE.Euler(1, -0.5, 1)}
  //         /> */}
  //       </XR>
  //     </Canvas>
  //   </>
  // );
};

export default Home;

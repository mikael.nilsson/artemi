import { Canvas } from "@react-three/fiber";
import Scene from "./Node";
import { getNode, getNodes } from "@/lib/nodes";
import { XR } from "@react-three/xr";

const Page = async ({ params }: { params: { nodeId: number } }) => {
  console.log({ nodeId: params.nodeId });
  const nodes = await getNodes();
  const node = params.nodeId;

  return <>{<Scene node={node} nodes={nodes} />}</>;
};

export default Page;

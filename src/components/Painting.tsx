"use client";

import * as THREE from "three";
import { Text, Html } from "@react-three/drei";
import { PropsWithChildren } from "react";
import { PaintingProps } from "@/types";

const CanvasArea = ({
  url,
  name,
  width,
  height,
  frameSize = new THREE.Vector2(0.005, 0.005),
}: {
  url: string;
  name: string;
  width: number;
  height: number;
  frameSize?: THREE.Vector2;
}) => {
  return (
    <Html
      className="image"
      position={[0, 0, 0.7]} // Position of image relative to frame
      scale={[0.05 - frameSize.x, 0.05 - frameSize.y, 0.01]}
      transform
      occlude
    >
      <img src={url} alt={name} width={(width / height) * 800} height={800} />
    </Html>
  );
};

const Sign = ({ text, position }: { text: string; position?: number[] }) => {
  position = position || [0.55, 1.5, 0];

  return (
    <Text
      maxWidth={0.1}
      anchorX="left"
      anchorY="top"
      position={[position[0], position[1], position[2]]}
      fontSize={0.1}
      color="#141414"
    >
      {text}
    </Text>
  );
};

interface FrameProps {
  name: string;
  frameScale: THREE.Vector3;
}

const Frame = ({
  name,
  frameScale,
  children,
}: PropsWithChildren<FrameProps>) => {
  return (
    <>
      <mesh
        name={name}
        scale={new THREE.Vector3(1, 1, 0.1)}
        position={[0, 1, 0]}
      >
        <boxGeometry args={[frameScale.x, frameScale.y]} />
        <meshStandardMaterial
          color="#151515"
          metalness={0.5}
          roughness={0.5}
          envMapIntensity={2}
        />
        <mesh raycast={() => null} scale={[1, 1, 1]} position={[0, 0, 0.2]}>
          {children}
        </mesh>
      </mesh>
    </>
  );
};

interface BoxProps {
  position: number[];
  rotation?: {
    x: number;
    y: number;
  };
}

// Props takes position of painting group
const Painting = ({
  url,
  name,
  position,
  rotation = new THREE.Euler(0, 0, 0),
  height,
  width,
  frameSize = new THREE.Vector2(0.005, 0.005),
}: PaintingProps) => {
  // const frame = useRef(null);

  const frameScale = new THREE.Vector3(1, height / width, 0.05);

  return (
    <>
      <group position={position} rotation={rotation}>
        <Frame name={name} frameScale={frameScale}>
          <CanvasArea
            url={url}
            name={name}
            width={width}
            height={height}
            frameSize={frameSize}
          />
        </Frame>
      </group>
    </>
  );

  // TODO: Får vi WebGL-error, gå tillbaka till denna
  // return (
  //   <>
  //     <group position={position} rotation={rotation}>
  //       <mesh
  //         name={name}
  //         scale={new THREE.Vector3(1, 1, 0.1)}
  //         position={[0, 1, 0]}
  //       >
  //         <boxGeometry args={[frameScale.x, frameScale.y]} />
  //         <meshStandardMaterial
  //           color="#151515"
  //           metalness={0.5}
  //           roughness={0.5}
  //           envMapIntensity={2}
  //         />
  //         <mesh raycast={() => null} scale={[1, 1, 1]} position={[0, 0, 0.2]}>
  //           <CanvasArea
  //             url={url}
  //             name={name}
  // width={width}
  // height={height}
  //             frameSize={frameSize}
  //           />
  //         </mesh>
  //       </mesh>
  //       {/* <Sign text={name} position={[0.55, 1.67, 0]} /> */}
  //     </group>
  //   </>
  // );
};

export default Painting;

import { Vector3, Vector2, Euler } from "three";

export interface NodeProps {
  id: number;
  position: number[];
  neighbours: number[];
  paintings: PaintingProps[];
}

export interface PaintingProps {
  url: string;
  name: string;
  position: Vector3;
  rotation: Euler;
  height: number;
  width: number;
  frameSize?: Vector2;
}

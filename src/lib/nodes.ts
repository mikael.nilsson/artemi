import * as THREE from "three";
import { prisma } from "./prisma";
import { positions } from "./positions";
import { PaintingProps, getPaintings } from "./paintings";
import { NodeProps } from "@/types";

interface Node {
  position: number[] | undefined;
  paintings: number[] | undefined;
  neighbours: Node[] | undefined;
}

// interface PaintingObject {
//   url: string;
//   name: string;
//   height: number;
//   width: number;
// }

// const paintings: PaintingObject[] = [
//   {
//     url: "https://drive.google.com/uc?export=view&id=101XMg7CdSzWUCVHlOa3oDTuQJZ8oZZam",
//     name: "Valborg",
//     width: 2022,
//     height: 2008,
//   },
//   {
//     url: "https://drive.google.com/uc?export=view&id=1FiLIXabD_XtY6BsScu_8HtLbIGqp4HtN",
//     name: "Deathblob",
//     width: 1704,
//     height: 2190,
//   },
//   {
//     url: "https://drive.google.com/uc?export=view&id=12SquiCupDtorUUXog4qrv7ZQ4TG-U80V",
//     name: "Tetradotoxin",
//     width: 835,
//     height: 815,
//   },
// ];

// const nodes: Node[] = [
//   {
//     position: new THREE.Vector3(0, 0, 0),
//     paintings: [
//       {
//         ...paintings[0],
//         ...positions[0],
//       },
//       {
//         ...paintings[1],
//         ...positions[1],
//       },
//       {
//         ...paintings[2],
//         ...positions[2],
//       },
//       {
//         ...paintings[0],
//         ...positions[3],
//       },
//       {
//         ...paintings[1],
//         ...positions[4],
//       },
//       {
//         ...paintings[2],
//         ...positions[5],
//       },
//     ],
//     neighbours: [1, 2, 3, 4, 5, 6],
//   },
//   {
//     position: new THREE.Vector3(0, 10, 0),
//     paintings: [
//       {
//         ...paintings[1],
//         ...positions[2],
//       },
//       {
//         ...paintings[2],
//         ...positions[0],
//       },
//     ],
//     neighbours: [0],
//   },
//   {
//     position: new THREE.Vector3(0, 0, 10),
//     paintings: [
//       {
//         ...paintings[2],
//         ...positions[3],
//       },
//       {
//         ...paintings[0],
//         ...positions[4],
//       },
//     ],
//     neighbours: [0],
//   },
//   {
//     position: new THREE.Vector3(10, 0, 0),
//     paintings: [],
//     neighbours: [0],
//   },
//   {
//     position: new THREE.Vector3(0, 0, -10),
//     paintings: [],
//     neighbours: [0],
//   },
//   {
//     position: new THREE.Vector3(0, -10, 0),
//     paintings: [],
//     neighbours: [0],
//   },
//   {
//     position: new THREE.Vector3(-10, 0, 0),
//     paintings: [],
//     neighbours: [0],
//   },
// ];

export const getNode = async (id: number) => {
  const dbNode = await prisma.node.findUnique({
    where: { id: Number(id) },
  });

  let node;

  if (dbNode) {
    node = {
      id: dbNode?.id,
      neighbours: dbNode.neighbours,
      position: dbNode.position,
      paintings: await getPaintings(dbNode.id),
    };
  }

  return node;
};

export const getNodes = async () => {
  const dbNodes = await prisma.node.findMany();
  let nodes: NodeProps[] = [];

  for (let i = 0; i < dbNodes.length; i++) {
    const node = {
      id: dbNodes[i].id,
      neighbours: dbNodes[i].neighbours,
      position: dbNodes[i].position,
      paintings: await getPaintings(dbNodes[i].id),
    };

    nodes.push(node);
  }

  console.log({ nodes });

  return nodes;
};

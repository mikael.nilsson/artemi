"use client";

import * as THREE from "three";
import { useState } from "react";
import { Sphere } from "@react-three/drei";
import Painting from "./Painting";
import { PaintingProps } from "@/types";

const NodeSphere = ({
  position,
  id,
  active = false,
  click,
  paintings,
}: {
  position: number[];
  id: number;
  active?: boolean;
  click?: (evt: any) => void;
  paintings?: PaintingProps[];
}) => {
  const [hovering, setHover] = useState(false);

  console.log("rendering nodesphere", id);

  const colors = [0, 0, 0];
  colors[id % 3] = 1;

  return (
    <>
      <Sphere
        name={id.toString()}
        castShadow
        scale={[0.3, 0.3, 0.3]}
        position={new THREE.Vector3(position[0], position[1], position[2])}
        onClick={click}
        onPointerOver={(event) => !active && setHover(true)}
        onPointerOut={(event) => setHover(false)}
      >
        <pointLight distance={60} intensity={4} color="blue" />
        <meshBasicMaterial
          color={
            hovering
              ? [0.2, 0.2, 1]
              : active
              ? [0, 1, 1]
              : [colors[0], colors[1], colors[2]]
          }
          toneMapped={false}
        />
      </Sphere>

      <>
        {paintings &&
          paintings.map((painting) => (
            <Painting
              key={painting.name}
              url={painting.url}
              name={painting.name}
              position={painting.position || new THREE.Vector3(0, 0, 0)}
              rotation={painting.rotation || new THREE.Euler(0, 0, 0)}
              height={painting.height}
              width={painting.width}
            />
          ))}
      </>
    </>
  );
};

export default NodeSphere;

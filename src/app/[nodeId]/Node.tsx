"use client";

import * as THREE from "three";
import { NodeProps } from "@/types";
import { CameraControls } from "@react-three/drei";
import { Canvas, ThreeEvent, useFrame, useThree } from "@react-three/fiber";
import { Fragment, useMemo, useRef, useState } from "react";
import NodeSphere from "@/components/NodeSphere";
import Cable from "@/components/Cable";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { XR } from "@react-three/xr";

interface Props {
  node: NodeProps;
  nodes: NodeProps[];
  click: (evt: ThreeEvent<MouseEvent>) => void;
  active?: boolean;
}

const Node = ({ node, nodes, click, active = false }: Props) => {
  return (
    <>
      <NodeSphere
        position={node.position}
        id={node.id}
        click={click}
        active={active}
        paintings={node.paintings}
      />
      {node.neighbours.map((neighbour, nIdx) => (
        <Fragment key={nIdx}>
          <Cable start={node.position} end={nodes[neighbour].position} />
        </Fragment>
      ))}
    </>
  );
};

const Nodes = ({ node, nodes }: { node: number; nodes: NodeProps[] }) => {
  const searchParams = useSearchParams();

  const nodeId = searchParams.get("nodeId") || "0";
  // const nodeId = node;

  const cameraRef = useRef<CameraControls>(null);
  const camera = useThree((state) => state.camera);
  const router = useRouter();

  const [activeNodeName, setActiveNodeName] = useState<string>(
    nodeId.toString()
  );
  const activeNode = useMemo(
    () => nodes.find((n) => n.id === Number(activeNodeName)),
    [activeNodeName, nodes]
  );
  const posArray = useMemo(
    () => (activeNode ? activeNode.position : [0, 0, 0]),
    [activeNode]
  );

  useFrame((state, delta) => {
    let pos: THREE.Vector3;
    let cameraPos;

    if (activeNodeName !== "") {
      pos =
        new THREE.Vector3(posArray[0], posArray[1], posArray[2]) ||
        new THREE.Vector3(0, 0, 0);
    } else {
      pos = new THREE.Vector3(0, 0, 0);
    }

    cameraPos = new THREE.Vector3(pos.x, pos.y, pos.z);

    cameraRef.current?.moveTo(cameraPos.x, cameraPos.y, cameraPos.z, true);
  });

  const click = (evt: ThreeEvent<MouseEvent>) => {
    evt.stopPropagation();
    console.log("redirecting to", evt.object.name);
    setActiveNodeName(evt.object.name);
    router.push("/0?nodeId=" + evt.object.name);
  };

  const deClick = () => {
    setActiveNodeName("0");
    router.push("/0?nodeId=0");
  };

  return (
    <>
      <group>
        {nodes.map((node) => (
          <Fragment key={node.id}>
            <Node
              node={node}
              nodes={nodes}
              click={click}
              active={node.id.toString() === activeNodeName}
            />
          </Fragment>
        ))}
        <CameraControls ref={cameraRef} camera={camera} />
      </group>
    </>
  );
};

const Scene = ({ node, nodes }: { node: number; nodes: NodeProps[] }) => {
  return (
    <Canvas shadows>
      <XR referenceSpace="local">
        <Nodes node={node} nodes={nodes} />
      </XR>
    </Canvas>
  );
};

export default Scene;

"use client";

import * as THREE from "three";
import { Cylinder } from "@react-three/drei";

const Cable = ({ start, end }: { start: number[]; end: number[] }) => {
  let distance = 0;

  const position = new THREE.Vector3(
    (end[0] + start[0]) / 2,
    (end[1] + start[1]) / 2,
    (end[2] + start[2]) / 2
  );

  let rotation = new THREE.Euler(0, 0, 0);
  // As we "know" that the cable will move only along axes, we can cheat a little
  if (Math.abs(position.x) > 0) {
    rotation.z = Math.PI / 2;

    distance = end[0] - start[0];
  }
  if (Math.abs(position.y) > 0) {
    rotation.y = Math.PI / 2;
    distance = end[1] - start[1];
  }
  if (Math.abs(position.z) > 0) {
    rotation.x = Math.PI / 2;
    distance = end[2] - start[2];
  }

  return (
    <mesh receiveShadow castShadow>
      <Cylinder
        position={position}
        scale={[0.05, distance, 0.05]}
        rotation={rotation}
      >
        <pointLight distance={60} intensity={4} color="lightblue" />
        <meshBasicMaterial color={[0.5, 0.5, 1]} toneMapped={false} />
      </Cylinder>
    </mesh>
  );
};

export default Cable;
